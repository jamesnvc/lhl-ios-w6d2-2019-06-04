//
//  DbManager.swift
//  SqlDemo
//
//  Created by James Cash on 04-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import Foundation
import FMDB

struct Student {
    let id: Int32
    let name: String
    let funFact: String?
}

struct Project {
    let id: Int32
    let title: String
    let studentId: Int32
}

class DbManager {

    private let db: FMDatabase

    init() {
        let dbPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!.appending("/demo.db")
        print("Db path: \(dbPath)")
        db = FMDatabase(path: dbPath)
        if !db.open() {
            print("Failed to open database")
            abort()
        }
        initDb()
        if countStudents() == 0 {
            seedDatabase()
        }
    }

    deinit {
        db.close()
    }

    func initDb() {
        let s = """
CREATE TABLE IF NOT EXISTS students (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  fun_fact TEXT
);

CREATE TABLE IF NOT EXISTS projects (
  id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  student_id INTEGER NOT NULL,
  FOREIGN KEY(student_id) REFERENCES students(id)
);
"""
        if !db.executeStatements(s) {
            print("Failed to create schema")
        }
    }

    func seedDatabase() {
        let s = """
INSERT INTO students (name, fun_fact)
VALUES
('James', 'Is tired'),
('πτθθτ', 'Is Greek'),
('🤔', 'Is a hipster'),
('Bob', NULL)
;
"""
        do {
            try db.executeUpdate(s, values: nil)
        } catch let err {
            print("Error seeding db: \(err.localizedDescription)")
        }
    }

    func countStudents() -> Int {
        let res = try! db.executeQuery("SELECT COUNT(*) AS count FROM students;", values: nil)
        res.next()
        return Int(res.int(forColumn: "count"))
    }

    func allStudents() -> [Student] {
        let res = try! db.executeQuery("SELECT * FROM students;", values: nil)
        var students = [Student]()
        while res.next() {
            students.append(
                Student(id: res.int(forColumn: "id"),
                        // force-unwrap name, because the NOT NULL constraint we put on that column guarantees that there must be a name
                        name: res.string(forColumn: "name")!,
                        funFact: res.string(forColumn: "fun_fact"))
            )
        }
        return students
    }

    func insertStudent(name: String, funFact: String?) {
        // WRONG WAY
//        db.executeUpdate("INSERT INTO students (name, fun_fact) VALUES ('\(name)', '\(funFact)'", withParameterDictionary: [:])
        //    insertStudent(name: "foo'; DROP TABLE students; --")
        db.executeUpdate(
            "INSERT INTO students (name, fun_fact) VALUES (:name, :fact);",
            withParameterDictionary: ["name": name, "fact": funFact as Any])
    }

    func addProject(title: String, for student: Student) {
        db.executeUpdate(
            "INSERT INTO projects (student_id, title) VALUES (:student_id, :title);",
            withParameterDictionary: ["title": title,
                                      "student_id": student.id])
    }

    func projects(for student: Student) -> [Project] {
        guard let res = db.executeQuery("SELECT * FROM projects WHERE student_id = :id", withParameterDictionary: ["id": student.id]) else { return [] }
        var projects = [Project]()
        while res.next() {
            projects.append(
                Project(id: res.int(forColumn: "id"),
                        title: res.string(forColumn: "title")!,
                        studentId: res.int(forColumn: "student_id"))
            )
        }
        return projects
    }

    func search(name: String) -> [String] {
        let q = """
SELECT students.name as student, group_concat(projects.title) as titles
FROM students
JOIN projects ON projects.student_id = students.id
WHERE students.name LIKE '%' || :name || '%'
GROUP BY students.id
"""
        guard let res = db.executeQuery(q, withParameterDictionary: ["name": name]) else { return [] }
        var info = [String]()
        while res.next() {
            info.append("\(res.string(forColumn: "student")!): \(res.string(forColumn: "titles")!)")
        }
        return info
    }
}
